from django.core.checks import messages
from django.db.models.expressions import Exists
from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib import messages, auth

# Create your views here.
def register(request):

    if request.method == "POST" :
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email = request.POST['email']
        username = request.POST['username']
        password = request.POST['password']
        retype_password = request.POST['retype_password']

        if password == retype_password :
            if User.objects.filter(username=username).exists():
                messages.error(request,'Username Taken')

            elif User.objects.filter(email=email).exists():
                messages.error(request,'Email already exits')

            else:
                User.objects.create_user(
                    first_name = first_name,
                    last_name = last_name,
                    email = email,
                    username = username,
                    password = password
                )
                User().save(User)
                return redirect('login')
        else:
            messages.error(request,'Password doesn\'t match')

    return render(request, 'register.html')

def login(request):

    if request.method == "POST" :
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username,password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            messages.error(request, 'Invalid username or password')

    return render(request, 'login.html')

def logout(request):
    auth.logout(request)
    return redirect('/')
    