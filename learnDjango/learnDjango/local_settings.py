DATABASES = {
    "default": {
        # Ends with "postgresql_psycopg2", "mysql", "sqlite3" or "oracle".
        "ENGINE": "django.db.backends.mysql",
        # DB name or path to database file if using sqlite3.
        "NAME": "learn_django",
        # Not used with sqlite3.
        "USER": "local_user",
        # Not used with sqlite3.
        "PASSWORD": "Local_user!@#123",
        # Set to empty string for localhost. Not used with sqlite3.
        "HOST": "localhost",
        "PORT": "3306",
    }
}