from django.db import models
from django.utils.text import slugify
# Create your models here.

class blogs(models.Model):
    title =  models.CharField(max_length=128)
    des =  models.TextField()
    img =  models.ImageField(upload_to='blog')
