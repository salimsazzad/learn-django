from django.shortcuts import render
from .models import blogs

# Create your views here.

def blog(request):
    blogPosts = blogs.objects.all()
    return render(request, 'blog.html', {'blogPosts': blogPosts})

def index(request):
    homeBlogDatas = blogs.objects.all()[:6]

    return render(request, 'index.html', {'homeBlogDatas': homeBlogDatas})

def blogPost(request):
    post = blogs.objects.get(id=int(request.GET['id']))

    return render(request, 'post.html',{'post': post})